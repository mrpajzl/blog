import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase'

import Home from './views/Home.vue'
import Login from './views/Login.vue'
import NewPost from './views/NewPost.vue'
import PostDetail from './views/PostDetail.vue'

const fb = require('@/firebaseConfig.js')

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/novy-clanek',
      name: 'NewPost',
      component: NewPost,
      meta: {
        requiresAuth: true
      }
    },

    {
      path: '/clanek/:post',
      name: 'PostDetail',
      component: PostDetail,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
  ],
})

// Authenticate routes that requires it
router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(x => x.meta.requiresAuth)
  const currentUser = firebase.auth().currentUser

  if (requiresAuth && !currentUser) {
    next('/login')
  } else if (requiresAuth && currentUser) {
    next()
  } else {
    next()
  }
})
export default router
