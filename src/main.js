import Vue from 'vue';
import App from './App.vue';
import router from './router';
import { store } from './store'

const fb = require('./firebaseConfig.js')

Vue.config.productionTip = false;

// Just a custom checker for authentication of user
fb.auth.onAuthStateChanged(user => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})
