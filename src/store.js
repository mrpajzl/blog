import Vue from 'vue'
import Vuex from 'vuex'

const fb = require('@/firebaseConfig.js')

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    posts: [],
    showLoader: true,
  },

  getters: {
    getPosts: (state, getters) => (id) => {
      return state.posts.filter(post => state.posts.indexOf(post) < id)
    },

    getPost: (state, getters) => (url) => {
      return state.posts.find(post => post.url === url)
    },
  },

  mutations: {
    ADD_POST: (state, post) => {
      state.posts.push(post)
    },
    SET_POSTS(state, posts) {
      this.state.posts = posts
    },
    SET_LOADER(state, data) {
      this.state.showLoader = data
    },
    setCurrentUser (state, val) {
      state.currentUser = val
    },
    setUserProfile (state, val) {
      state.userProfile = val
    },
  },

  actions: {
    loadPosts({ commit }) {
      fb.postsCollection.get().then((querySnapshot) => {
        const postsArray = []
        querySnapshot.forEach((doc) => {
          const data = {
            id: doc.id,
            title: doc.data().title,
            url: doc.data().url,
            content: doc.data().content,
            author: doc.data().author,
            authorNick: doc.data().authorNick,
            createdAt: doc.data().createdAt,
          }
          postsArray.push(data)
        })
        commit('SET_POSTS', postsArray)
        commit('SET_LOADER', false)
      })
    },
    fetchUserProfile ({ commit, state }) {
      fb.usersCollection.doc(state.currentUser.uid).get().then(res => {
        commit('setUserProfile', res.data())
      }).catch(err => {
        console.log(err)
      })
    }
  },
})

fb.auth.onAuthStateChanged((user) => {
  if (user) {
    store.commit('setCurrentUser', user)
    store.dispatch('fetchUserProfile')

    fb.usersCollection.doc(user.uid).onSnapshot((doc) => {
      store.commit('setUserProfile', doc.data())
    })
  }
})
