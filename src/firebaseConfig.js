import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/auth';
import 'firebase/firestore';

// firebase init goes here
const config = {
  apiKey: 'AIzaSyBDC2rGwDqbdE8W2xwS0PoUvlqNHrE_TkI',
  authDomain: 'lorem-ipsum-blog.firebaseapp.com',
  databaseURL: 'https://lorem-ipsum-blog.firebaseio.com',
  projectId: 'lorem-ipsum-blog',
  storageBucket: 'lorem-ipsum-blog.appspot.com',
  messagingSenderId: '908373248685',
  timestampsInSnapshots: true,
};
firebase.initializeApp(config);

// firebase utils
const db = firebase.firestore();
const auth = firebase.auth();
const storage = firebase.storage();
const currentUser = auth.currentUser;

// firebase collections
const usersCollection = db.collection('users');
const postsCollection = db.collection('posts');

export {
  db,
  auth,
  storage,
  currentUser,
  usersCollection,
  postsCollection,
};
