import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import BlogPost from '@/components/BlogPost.vue';

describe('BlogPost.vue', () => {
    let propsData;

    beforeEach(() => {
        propsData = {
            post: {
                title: "Test title",
                content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam totam aliquid aspernatur inventore nesciunt natus, saepe obcaecati veniam quasi eos expedita asperiores eius! Non obcaecati corrupti fugit ab consequatur quam.",
                author: "John Smith",
                authorNick: "johny901",
                url: "test-title",
            }
        }
    })

    it('renders properly title of blog post', () => {
        const wrapper = shallowMount(BlogPost, { propsData })
        expect(wrapper.find('h3.is-size-3').text()).to.equal(propsData.post.title)
    });
    it('should render blog post content in propper trimmed length', () => {
        const wrapper = shallowMount(BlogPost, { propsData })
        expect(wrapper.find('h3.is-size-3').text().length <= 700).to.be.true
    });
});
